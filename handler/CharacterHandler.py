import jieba

from handler.StaticValue import includes, excludes
from handler.StopWordsRepository import repository


class CharacterHandler:
    def __init__(self, novel_path):
        self.novel_path = novel_path
        self.characters = []

    def get_characters(self, top=20):
        novel_txt = open(self.novel_path, "r", encoding='utf-8').read()
        words = jieba.lcut(novel_txt)
        for word in includes:
            words.append(word)

        counts = {}
        for word in words:
            if len(word) == 1:  # 排除单个字符的分词结果
                continue
            else:
                counts[word] = counts.get(word, 0) + 1

        for word in excludes:
            del (counts[word])
        items = list(counts.items())
        items.sort(key=lambda x: x[1], reverse=True)

        top_characters = []
        stop_words = repository.stop_words
        for i in range(top):
            word, count = items[i]
            if word in stop_words:
                continue

            # prefix
            if word[:1]  in stop_words:
                continue

            # suffix
            if word[-1:] in stop_words:
                continue

            top_characters.append(word)
            print("{0:<10}{1:>5}".format(word, count))

        for element in includes:
            if element not in top_characters:
                top_characters.append(element)
                print("{0:<10}{1:>5}".format(element, 0))

        return top_characters
