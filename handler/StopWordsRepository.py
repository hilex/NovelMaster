from handler.StaticValue import data_path


class StopWordsRepository:
    def __init__(self, data_path):
        stop_words_file = open(data_path + "\\stop_words.txt", 'r')

        self.stop_words = list()
        for line in stop_words_file.readlines():
            line = line.strip()  # 去掉每行末尾的换行符
            self.stop_words.append(line)
        stop_words_file.close()


repository = StopWordsRepository(data_path)
