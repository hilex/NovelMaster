import gensim.models as w2v
import jieba

from handler.Intimacy import Intimacy
from handler.StopWordsRepository import repository


class NovelHandler:
    def __init__(self, novel_path, data_path, novel_name, character_names):
        self.novel_path = novel_path
        self.data_path = data_path
        self.novel_name = novel_name

        self.character_names = character_names
        for character in character_names:
            jieba.add_word(character)

        self.model = None

    def build_model(self):
        stop_words = repository.stop_words

        seg_novel = []
        novel = open(self.novel_path, 'r', encoding='utf-8')
        print("Waiting...")
        line = novel.readline()
        while line:
            line_1 = line.strip()
            out_str = ''
            line_seg = jieba.cut(line_1, cut_all=False)
            for word in line_seg:
                if word not in stop_words:
                    if word != '\t':
                        if word[:2] in self.character_names:
                            word = word[:2]
                        if word[:3] in self.character_names:
                            word = word[:3]
                        out_str += word
                        out_str += " "
            if len(str(out_str.strip())) != 0:
                seg_novel.append(str(out_str.strip()).split())
            line = novel.readline()

        self.model = w2v.Word2Vec(sentences=seg_novel, vector_size=200, window=7, min_count=5, workers=8, sg=1)
        self.model.save(self.data_path + '//' + self.novel_name + '.model')

    def get_character_intimacy(self, character, top):
        if self.model is None:
            print("model does not exit!")
            return

        intimacies = []
        for other_character in self.character_names:
            if character == other_character:
                continue

            intimacy = Intimacy(character, other_character, self.model.wv.similarity(character, other_character))
            intimacies.append(intimacy)

        self._sort(intimacies)
        print('\n'+str(character) + ':')
        for i in range(top):
            print(intimacies[i].character2 + ": " + str(intimacies[i].similarity))

    def _sort(self, intimacies):
        for i in range(len(intimacies) - 1):
            for j in range(len(intimacies) - i - 1):
                if intimacies[j].similarity < intimacies[j + 1].similarity:
                    temp = intimacies[j]
                    intimacies[j] = intimacies[j + 1]
                    intimacies[j + 1] = temp
        return intimacies
