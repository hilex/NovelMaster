from handler.StaticValue import novel_path, data_path, novel_name
from handler.CharacterHandler import CharacterHandler
from handler.NovelHandler import NovelHandler


top_characters = CharacterHandler(novel_path).get_characters(top=100)
handler = NovelHandler(novel_path, data_path, novel_name, top_characters)
handler.build_model()
handler.get_character_intimacy(character="鸿俊", top=8)
handler.get_character_intimacy(character="陆许", top=8)
handler.get_character_intimacy(character="重明", top=8)
handler.get_character_intimacy(character="阿史那琼", top=8)
